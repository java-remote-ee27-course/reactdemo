# React demo

A React demo app on Next.js

<img src = "./assets/demo1_8.04.24.png" alt="page-demo" />

## Functionalities, components
- Routing
- page.js
- Components:
    - header
    - button
    - dancers-list
    - navigation
    - page-header
    - dancers
    - gallery


