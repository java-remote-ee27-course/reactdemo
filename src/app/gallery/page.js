import React from 'react';
import styles from './gallery.module.css';

const Gallery = () => (
  <div className={styles.Gallery} data-testid="Gallery">
    Gallery Component
  </div>
);

Gallery.propTypes = {};

export default Gallery;
