export default function GalleryLayout({ children }) {
    return <section>{children}</section>
}