import React from 'react';
import styles from './page-header.module.css';



function createTitle(title) {
  if (title) {
    return title;
  }
  else {
    return "Some beautiful stuff";
  }
}

const PageHeader = ({title}) => (
  <div className={styles.PageHeader} data-testid="PageHeader">
        <h1 className='text mt-3'>
          {createTitle(title)}
        </h1>
  </div>
);

PageHeader.propTypes = {};

export default PageHeader;