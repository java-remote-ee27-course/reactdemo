import React from 'react';
import styles from './header.module.css';
import Navigation from '../navigation/navigation.js'; 

const myLinks = [
  {
    page: "/", 
    pageName: "Home", 
    navItemStyle: "nav-item", 
    navItemLink: "nav-link active",
  },
  {
    page: "/gallery", 
    pageName: "Gallery", 
    navItemStyle: "nav-item", 
    navItemLink: "nav-link",
  },
  {
    page: "/dancers", 
    pageName: "Dancers", 
    navItemStyle: "nav-item", 
    navItemLink: "nav-link",
  }, 
];

const Header = () => (
  <div className={styles.Header} data-testid="Header">
    <Navigation linksToMap = {myLinks} />
  </div>
);

Header.propTypes = {};

export default Header;