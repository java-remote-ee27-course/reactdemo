import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import DancersList from './DancersList';

describe('<DancersList />', () => {
  test('it should mount', () => {
    render(<DancersList />);
    
    const dancersList = screen.getByTestId('DancersList');

    expect(dancersList).toBeInTheDocument();
  });
});