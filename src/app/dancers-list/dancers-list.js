"use client"

import React from 'react';
import { useState } from 'react';
import styles from './dancers-list.module.css';
import Button from '../button/button';

function DancersList({ names}) {

  const [likes, setLikes] = useState(0);
  
  function handleClick() {
    setLikes(likes + 1);
  }
 
  return (
    <div className={styles.DancersList} data-testid="DancersList">
      <ul>
        {names.map((name) => (
          <li key={name}>
            {name} <Button className="btn btn-success m-2" onClick={handleClick} text = "Like" /> 
          </li>
        ))}
      </ul>
    </div>
  );
}

DancersList.propTypes = {};

export default DancersList;
