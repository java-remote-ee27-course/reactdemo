import React, { lazy, Suspense } from 'react';

const LazyDancersList = lazy(() => import('./DancersList'));

const DancersList = props => (
  <Suspense fallback={null}>
    <LazyDancersList {...props} />
  </Suspense>
);

export default DancersList;
