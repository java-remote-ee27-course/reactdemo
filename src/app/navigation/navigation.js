"use client"

import React from 'react';
import { useState } from 'react';
import styles from './navigation.module.css';
import Link from 'next/link';


function Navigation ({linksToMap}) {
  // adding the states 
  const [isActive, setIsActive] = useState(false);

  //add the active class
  const toggleActiveClass = () => {
    setIsActive(!isActive);
  };

  const removeActive = () => {
    setIsActive(false)
  }

  return (
  <div className={`${styles.Navigation} ${isActive ? styles.active : ''}`} data-testid="Navigation">
    <nav className="navbar navbar-expand-lg bg-body-secondary sticky-top">
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
    </button>

      <div className="container-fluid collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">

          { linksToMap.map((data) => (
            <li key={data.page} onClick={removeActive} className="nav-item">
              <Link href={data.page} className={data.navItemLink}>{data.pageName}</Link>
            </li>
          ))}
        </ul>
      </div>
    </nav>
  </div>
  )
};

Navigation.propTypes = {};

export default Navigation;
