"use client"

import React from 'react';
import { useState } from 'react';
import styles from './button.module.css';



function setText(text){
  if(text){
    return text;
  }
  return "Click me!"
}


function Button  ({ color, className, text}) {
  const [likes, setLikes] = useState(0);

  function handleClick() {
    setLikes(likes + 1);
  }

  return (
  <div className={styles.Button} data-testid="Button">
    <button type="button" color={color} className={className} onClick={handleClick}>{setText(text)}</button>
    <span> ❤️ {likes}</span>
  </div>
  );
}

Button.propTypes = {};

export default Button;
