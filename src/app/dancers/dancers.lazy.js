import React, { lazy, Suspense } from 'react';

const LazyDancers = lazy(() => import('./Dancers'));

const Dancers = props => (
  <Suspense fallback={null}>
    <LazyDancers {...props} />
  </Suspense>
);

export default Dancers;
