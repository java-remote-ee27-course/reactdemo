import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Dancers from './Dancers';

describe('<Dancers />', () => {
  test('it should mount', () => {
    render(<Dancers />);
    
    const dancers = screen.getByTestId('Dancers');

    expect(dancers).toBeInTheDocument();
  });
});