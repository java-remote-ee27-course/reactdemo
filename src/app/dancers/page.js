import React from 'react';
import styles from './dancers.module.css';

const Dancers = () => (
  <div className={styles.Dancers} data-testid="Dancers">
    Dancers Component
  </div>
);

Dancers.propTypes = {};

export default Dancers;
