"use client"
import { useState } from 'react';
import PageHeader from './page-header/page-header.js';
import Button from './button/button.js';
import DancersList from './dancers-list/dancers-list.js';

function Paragraph({ value }) {
  return (<p> {value ? value : `Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. `} </p>);
}

export default function Home({Button}) {
  const names = ['La Lupi', 'Sara Baras', 'Farruquito'];
  const [likes, setLikes] = useState(0);

  function handleClick() {
    setLikes(likes + 1);
  }


  return (
    <div>
      <main className="container">

        <PageHeader title="A nice little page about dances" />
        <Paragraph />
        <Paragraph value="This is a paragraph" />
        <PageHeader />

        <Paragraph />
        <DancersList names = {names}> </DancersList>
      </main>
    </div>
  );
}

